package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@Value("${sample.profile.message}")
	private String contextPath;
	
	@RequestMapping("/")
	public String DemoMessage() {
		return contextPath;
	}
}
